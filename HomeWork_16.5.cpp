﻿#include <iostream>

int main()
{
	int Sum = 0;
	const int N = 5;
	int numbers[N][N];

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {

			numbers[i][j] = i + j;
			std::cout << numbers[i][j] << ' ';

			if (j == N-1) 
				std::cout << '\n';
			
		}
	}

	for (int j = 0; j < N; j++) {
		int i = 14 % N;
		Sum += numbers[i][j];
	}

	std::cout << Sum;

	return 0;
}

